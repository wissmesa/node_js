export const jsonTest = [
    {
      "id_cuestionario": 1,
      "descripcion": "Cuestionario HCMI",
      "status": "A",
      "fecha_status": "12/10/2020",
      "sesiones": [
        {
          "id_seccion": 1,
          "descripcion": "sesion numero 1",
          "tipo_numeracion": "numero",
          "orden": "1",
          "status": "visible",
          "preguntas": [
            {
              "id_pregunta": 1,
              "sec_pregunta": "1",
              "descripcion": "pregunta 1",
              "padreDep": 0,
              "cod_lista": "001",
              "cod_consulta": "",
              "tipo": "SIMPLE",
              "orden": 1,
              "sexo": "mixto",
              "visible_planilla": "",
              "visible_pantalla": ""
            },
            {
              "id_pregunta": 2,
              "sec_pregunta": "2",
              "descripcion": "pregunta 2",
              "padreDep": 1,
              "lista": "",
              "consulta": "",
              "tipo": "SIMPLE",
              "orden": 1,
              "sexo": "mixto",
              "visible_planilla": "",
              "visible_pantalla": "",
              "categoria": [
                {
                  "value": "A",
                  "descripcion": "HIPERTENSIÓN"
                }
              ]
            }
          ]
        },
        {
          "id_seccion": 2,
          "descripcion": "sesion numero 2",
          "tipo_numeracion": "numero",
          "orden": "2",
          "status": "visible",
          "preguntas": [
            {
              "id_pregunta": 1,
              "sec_pregunta": "1",
              "descripcion": "pregunta 1",
              "padreDep": "N",
              "lista": "cardiologia",
              "consulta": "",
              "tipo": "SIMPLE_LISTA",
              "orden": 1,
              "sexo": "mixto",
              "visible_planilla": "",
              "visible_pantalla": "",
              "categoria": [
                {
                  "value": "A",
                  "descripcion": "HIPERTENSIÓN"
                },
                {
                  "value": "B",
                  "descripcion": "ARRITMIA"
                },
                {
                  "value": "C",
                  "descripcion": "INFARTO"
                }
              ]
            }
          ]
        }
      ]
    }
  ]

 