const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
// const { getFiles } = require('./middleware/GetFiles');

const app = express();
const port = 3000;

const arregloArmado = [
  "file,text,number,hex",
  "file,text,number,hex\ntest2.csv,wZriz\ntest2.csv,DUhIEVDPkBVMhZioY,461967207,c5d591d7b7ff82c0b85b06430ae2e47b",
  "file,text,number,hex\ntest3.csv,HMgYD\ntest3.csv,Vp,84328,47258d0c01a2ecf036da95759aa0de98\ntest3.csv,JbqCWtYKdpbmsoksjdCuIMqfzVm,00179538,82d1b4a79f88b18d21b0e5aa3ad4d810\ntest3.csv,SdCeolpLSt,0,4139d830d36d2c99182352ac1b9682b9",
  "file,text,number,hex\ntest18.csv,kgpm\ntest18.csv,IiHzNAOT,8870,jz4f4a84c63f05d47179240f4aa780\ntest18.csv,GV,34139858,jzb89aecb2535c702bf3497280055f\ntest18.csv,jk,26741,jza38779b8a9d7b4e327fe793b916e\ntest18.csv,Ypv,5632,jzaac840443ef9dd7ae57c5a8dc525\ntest18.csv,JEmQBC,893,jz444d675d54ec98e890910c55d594",
  "file,text,number,hex\ntest6.csv,fUqyV\ntest6.csv,hQYNNWNqJ,94585o,bf5ee65f822b1e2c79492df4b4e2ea99\ntest6.csv,TMwmMuRJeuEUnzpoGPAhnBTdia,12937o,8b46f0e250b11fec946aa421217b5f26\ntest6.csv,t,126758859o,84cd5ff8ba8d450cc96e2ba74398863c\ntest6.csv,OzhuuqnURNLCwbKDOtEXBT,864454o,663cea3c196a8ea0cd97dd03559e4fd8\ntest6.csv,FPgg,3960738o,4d7e58dbb147a2c7ac2984d466c9f282\ntest6.csv,mea,839018o,9dad2c26538c61e21db7e51322afe6c0\ntest6.csv,pwJVVCLUN,5116213o,6216379240e5bb784c1dba7a1b26bbb0\ntest6.csv,ZyxgufYMmlHNB,3o,2dd03db965cb627ed40eab834eea90e6,,\ntest6.csv,ZBkwi\ntest6.csv,UpupLYqeZDSABAFJJVxMD,8296o,3c0b4ba98632aa85668c069aa53f6d4c\ntest6.csv,lqsM,895o,d4ebad25f37e246f47fd0dad18171d9e",
  "file,text,number,hex\ntest9.csv,RpOwQ\ntest9.csv,NJoOfGM,054183563,5a2fe72516a2d4089e4e5e3e73b3d779\ntest9.csv,GegrEsNoojWXdSBoNum,4660116821,25edf5826a8bdbfda6d49e40440f40f7\ntest9.csv,qazDtZrLAaihdiWh,52866258,e44344c2a82f6cde415069f2b16b515b\ntest9.csv,QgLhJEutL,14232622366219861148458129368660,5039272a238311bae3f5fc44bdcf6c5d\ntest9.csv,rhlAMi,46989832,5cf7fbe16e26782f4d34a8684f9b11c6\ntest9.csv,fGndUtD,8770008,ba5d4ddf9e4ebe0cc6958d259b4b9a83\ntest9.csv,Npx,90279339,18673eb8e76e3ab8dbef66a7710f9f74\ntest9.csv,UuDFXFsUHMGCSKmBJPVSbwDCJwfC,6,b2b3e013d7083106e50db81a4d488245,,\ntest9.csv,lcteb\ntest9.csv,QBbLKHeyXOzurLJkipLSVycN,7190189,81d822c720cb67c8f5a08571e4f4ce65\ntest9.csv,dgZTJjHYmHLVUdztKCMoqkJwu,503,bb029057da1bd6e3c448844283049d27\ntest9.csv,jgeVdRvyqdoaVCKLVZOHvcfUotTW,2,a59308670942a2393951e12bd817f6c3\ntest9.csv,HyF,374204992,5cd85b3a1f3187a2add3550acca6d2cb",
  "file,text,number,hex\ntest15.csv,CrSV\ntest15.csv,GrrmVbRAApqJilP,,\ntest15.csv,mgUEVmmANrDFftsZNOKjsRBVbUU,,\ntest15.csv,Irf,,\ntest15.csv,QlfYQhliHOoRRpSrxwLooRXmturCpi,,\ntest15.csv,tQZwYBVncPuLLcw,,\ntest15.csv,jZXGFEMqdbuvgPNvhiZ,,\ntest15.csv,uPS,,\ntest15.csv,vkCPOQ,,,,\ntest15.csv,IN,,\ntest15.csv,kECalqua,,\ntest15.csv,fbSyUR,,\ntest15.csv,MFjQGhIjpWQvMSTvsgjnNZABlYsm,,\ntest15.csv,IwBmtebDxZNntWeQ,,\ntest15.csv,WHK,,\ntest15.csv,VzommZYOZYMyMIAkOppNjz,,"
]

app.use(bodyParser.json());

const accessToken = 'aSuperSecretKey'; // Replace with your actual token

app.post('/echo', async (req, res) => {
  try {
    const response = await fetch('https://echo-serv.tbxnet.com/v1/secret/files', {
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    });

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const data = await response.json();
    let array = [];
    data.map((file)=> {
      
    })


    res.json(data);   
 // Send the fetched data as the response

  } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'Error fetching data' }); // Send a generic error response
  }
});



app.post('/processFiles', async (req, res) => {
  // const { files } = req.body; // Obtén la lista de archivos del cuerpo de la solicitud
  const filesArray = [
      "test1.csv",
      "test2.csv",
      "test3.csv",
      "test18.csv",
      // "test4.csv",  TIENEN ERRORES
      // "test5.csv",  TIENEN ERRORES
      "test6.csv",
      "test9.csv",
      "test15.csv"
    ]

    try {
      const results = [];
      for (const file of filesArray) {
        const response = await fetch(`https://echo-serv.tbxnet.com/v1/secret/file/${file}`, {
          headers: {
            'Authorization': `Bearer ${accessToken}`
          }
        });
  
        if (!response.ok) {
          throw new Error(`Error fetching data for file ${file}`);
        }
  
        const data = await response.text(); // Obtener el texto de la respuesta
        const lines = data.split('\n');
        const formattedData = lines.map(line => line.split(',')).join('\n');
        results.push(formattedData);
      }
  
      res.json(results);
    } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'Error processing files' });
  }
});

app.post('/arrayTranformedToJSON', async (req, res) => {
    try {
      function parseData(data) {
        const result = [];
        data.forEach(item => {
          const lines = item.split('\n');
          const headers = lines[0].split(',');
          const currentFile = headers[0];
      
          const fileData = {
            file: currentFile,
            lines: []
          };
      
          lines.slice(1).forEach(line => {
            const values = line.split(',');
            const lineData = {};
            headers.slice(1).forEach((header, index) => {
              lineData[header] = values[index+1];
            });
            fileData.lines.push(lineData);
          });
      
          result.push(fileData);
        });
        return result;
      }
      const parsedData = parseData(arregloArmado);
      res.json(parsedData);
    } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'Error processing files' });
  }
});


app.listen(port, () => {
    console.log(`Servidor escuchando en el puerto ${port}`);
});